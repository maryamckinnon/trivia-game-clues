from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

router = APIRouter()


class GameIn(BaseModel):
    id: int


class GameOut(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool


class GameWithWinCount(GameOut):
    total_amount_won: int | None


class Games(BaseModel):
    games: list[GameWithWinCount]


class Message(BaseModel):
    message: str
    

@router.get("/api/games/{game_id}",
    response_model= GameWithWinCount)
def get_game(game_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT 
                g.id
                , g.episode_id
                , g.aired
                , g.canon
                , SUM(g.id) AS total_amount_won
                FROM games AS g
                INNER JOIN clues AS cl
                    ON (g.id = cl.game_id)
                WHERE g.id = %s
                GROUP BY 1,2,3;
                """, [game_id],
            )

            row = cur.fetchone()
            if row is not None:
                game = {}
                game_fields=[
                    "id",
                    "episode_id",
                    "aired",
                    "canon",
                    "total_amount_won"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in game_fields:
                        game[column.name] = row[i]

            return game



@router.post("api/custom-games")
def create_game():
    with psycopg.connect(autocommit=True) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT 
                cl.id
                , cl.answer
                , cl.question
                , cl.value
                , cl.invalid_count
                FROM clues AS cl
                WHERE clues.canon = true
                ORDER BY RANDOM(30);
                """,
            )
            with conn.transaction():
                cur.execute(
                    """
                    INSERT INTO game_definitions (created_on)
                    VALUES (CURRENT_TIMESTAMP)
                    """
                    )
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT gd.id
                    , gd.created_on
                    , gd.clues
                    FROM game_definitions AS gd
                    """
                )
            row = cur.fetchall()
            if row is not None:
                clue = {}
                clue_fields=[
                    "id",
                    "answer",
                    "question",
                    "value",
                    "invalid_count",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                
                category = {}
                category_fields=[
                    "id",
                    "title",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in category_fields:
                        category[column.name] = row[i]
                    clue["category"] = category
                