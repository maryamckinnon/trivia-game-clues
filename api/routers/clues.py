from fastapi import APIRouter, Response, status
from pydantic import BaseModel
from routers.categories import CategoryOut, Message
# from routers.games import GameOut
import psycopg

router = APIRouter()

class ClueIn(BaseModel):
    id: int


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    # game_id: GameOut
    category: CategoryOut
    canon: bool


class Clues(BaseModel):
    page_count: int
    clues: list[ClueOut]


@router.get("/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
    )
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT 
                clues.id
                , clues.answer
                , clues.question
                , clues.value
                , clues.invalid_count
                , clues.canon
                , categories.id
                , categories.title
                , categories.canon
                
                FROM clues
                INNER JOIN categories
                    ON (clues.category_id = categories.id)
                WHERE clues.id = %s;
                """, [clue_id],
            )

            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            if row is not None:
                clue = {}
                clue_fields=[
                    "id",
                    "answer",
                    "question",
                    "value",
                    "invalid_count",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                
                category = {}
                category_fields=[
                    "id",
                    "title",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in category_fields:
                        category[column.name] = row[i]
                    clue["category"] = category

            return clue



@router.get("/api/random-clue",
    response_model=ClueOut,
    )
def get_random_clue(valid: bool=True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if valid == True:
                cur.execute(
                    """
                    SELECT
                    clues.id
                    , clues.answer
                    , clues.question
                    , clues.value
                    , clues.invalid_count
                    , categories.id
                    , categories.title
                    , categories.canon
                    , clues.canon
                    FROM clues
                    INNER JOIN categories
                        ON (clues.category_id = categories.id)
                    WHERE clues.invalid_count = 0
                    ORDER BY RANDOM();
                    """
                )

            else:
                cur.execute(
                    """
                    SELECT
                    clues.id
                    , clues.answer
                    , clues.question
                    , clues.value
                    , clues.invalid_count
                    , categories.id
                    , categories.title
                    , categories.canon
                    , clues.canon
                    FROM clues
                    INNER JOIN categories
                        ON (clues.category_id = categories.id)
                    ORDER BY RANDOM();
                    """
                )

            row = cur.fetchone()
            if row is not None:
                clue = {}
                clue_fields=[
                    "id",
                    "answer",
                    "question",
                    "value",
                    "invalid_count",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                
                category = {}
                category_fields=[
                    "id",
                    "title",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in category_fields:
                        category[column.name] = row[i]
                    clue["category"] = category

            return clue




# @router.get("/api/clues", response_model=Clues)
# def get_clues(page: int= 0):
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 SELECT 
#                 clues.id
#                 , clues.answer
#                 , clues.question
#                 , clues.value
#                 , clues.invalid_count
#                 , clues.canon
#                 , categories.id
#                 , categories.title
#                 , categories.canon
                
#                 FROM clues
#                 INNER JOIN categories
#                     ON (clues.category_id = categories.id)
#                 LIMIT 100 OFFSET %s
#                 """, [page*100],
#             )

#             row = cur.fetchall()
#             clue = {}
#             clue_fields=[
#                 "id",
#                 "answer",
#                 "question",
#                 "value",
#                 "invalid_count",
#                 "canon"
#             ]
#             for i, column in enumerate(cur.description):
#                 if column.name in clue_fields:
#                     clue[column.name] = row[i]
            
#             category = {}
#             category_fields=[
#                 "id",
#                 "title",
#                 "canon"
#             ]
#             for i, column in enumerate(cur.description):
#                 if column.name in category_fields:
#                     category[column.name] = row[i]
#                 clue["category"] = category

#             return clue



@router.delete("/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
    )
def remove_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE clues.id = %s;
                """, [clue_id],
            )
            cur.execute(
                """
                SELECT 
                clues.id
                , clues.answer
                , clues.question
                , clues.value
                , clues.invalid_count
                , clues.canon
                , categories.id
                , categories.title
                , categories.canon
                
                FROM clues
                INNER JOIN categories
                    ON (clues.category_id = categories.id)
                WHERE clues.id = %s;
                """, [clue_id],
            )
            
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            if row is not None:
                clue = {}
                clue_fields=[
                    "id",
                    "answer",
                    "question",
                    "value",
                    "invalid_count",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                
                category = {}
                category_fields=[
                    "id",
                    "title",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in category_fields:
                        category[column.name] = row[i]
                    clue["category"] = category

            return clue